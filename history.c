// History functions of the shell
// Petteri Mustonen, 2014

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "uglyshell.h"

FILE* historyfile;
int cmdcount;

void init_history()
{
    historyfile = tmpfile();
    cmdcount = 1;
}

void write_history(char* buf)
{
    fprintf(historyfile, "%d %s\n",cmdcount, buf);
    cmdcount++;
}

void print_history()
{
    char buffer[MAX_LINE];
    rewind(historyfile);
    while(fgets(buffer, MAX_LINE, historyfile))
        puts(buffer);
    fseek(historyfile, 0, SEEK_END);
}

char* get_history(int count)
{
    char buffer[MAX_LINE];
    char* commandline = malloc(MAX_LINE * sizeof(char));
    int i = 1;
    
    rewind(historyfile);
    
    for(i=0; i != count; i++)
    {
        fgets(buffer, MAX_LINE, historyfile);
    }
    
    if (buffer[strlen(buffer) - 1] == '\n')
       buffer[strlen(buffer) - 1] = 0; //strip the newline
       
    strcpy(commandline, strpbrk(buffer, " "));
    
    fseek(historyfile, 0, SEEK_END);
    
    return commandline;
}
