#ifndef PARSER_H
#define PARSER_H

#include "uglyshell.h"

command* parse(char* buf);
command* parse_cmnd(char* buf);

#endif