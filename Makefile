all: uglyshell 

uglyshell: uglyshell.o builtins.o parser.o history.o
	gcc -Wall uglyshell.o builtins.o parser.o history.o -o ush 

uglyshell.o: uglyshell.c uglyshell.h
	gcc -c -Wall uglyshell.c
	
builtins.o: builtins.c builtins.h
	gcc -c -Wall builtins.c
	
parser.o: parser.c parser.h
	gcc -c -Wall parser.c

history.o: history.c history.h
	gcc -c -Wall history.c
	
clean:
	rm -rf *o uglyshell 
