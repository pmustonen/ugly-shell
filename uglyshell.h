#ifndef UGLYSHELL_H
#define UGLYSHELL_H

//constant definitions
#define MAX_LINE 1024
#define MAX_ARGS 32

typedef struct s_command {
    char* name;
    char* argv[MAX_ARGS];
    char* output;
    char* input;
    char backgnd;
    struct s_command* next; //next command to be executed in pipeline
} command;

//function prototypes
void launch(command* cmnd);
pid_t spawn(command* cmnd, int input, int output);
int free_command(command* cmnd);

#endif