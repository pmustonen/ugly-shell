#ifndef HISTORY_H
#define HISTORY_H

#include "uglyshell.h"

void init_history();
void write_history(char* buf);
void print_history();
char* get_history(int);



#endif