/* UGLY SHELL, the assignment of course BM40A0400 Systeemiohjelmointi
 * 
 * Petteri Mustonen, 2014
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>

#include "uglyshell.h"
#include "builtins.h"
#include "parser.h"
#include "history.h"

void child_handler(int signum);

int main(void)
{
    char cwd[MAX_LINE];
    char buf[MAX_LINE];
    command* cmnd = NULL;
    
    init_history();
    
    //get current path
    getcwd(cwd, sizeof(cwd));
    printf("%s%% ", cwd);
    
    while (fgets(buf, MAX_LINE, stdin))
    {
        if (buf[strlen(buf) - 1] == '\n')
            buf[strlen(buf) - 1] = 0; //strip the newline
            
        write_history(buf);
        cmnd = parse(buf);
        
        if (cmnd)
        {
            //if not builtin command try to spawn a process
            if (exec_builtin(cmnd) == 1)
                launch(cmnd);       
                free_command(cmnd);
        }
        
        getcwd(cwd, sizeof(cwd));
        printf("%s%% ", cwd);
    }
    
    return 0;
}

void launch(command* cmnd)
{
    //this functions launches a pipeline (or a single process if that's the case)
    int status;
    pid_t pid;
    int pipeline[2];
    int input = STDIN_FILENO;
    int output;
    
    //executes all the commands in pipeline, cmnd is a linked list.
    for (; cmnd; cmnd = cmnd->next)
    {
        if (cmnd->next)
        {
            if (pipe(pipeline) < 0)
            {
                perror("Pipe");
                exit(EXIT_FAILURE);
            }
            output = pipeline[1];
        }
        else
            output = STDOUT_FILENO;
        
        //input redirection
        if (cmnd->input)
        {
            input = open(cmnd->input, O_RDONLY);
                    
            if (input < 0)
            {
                perror("error opening input stream:");
                exit(EXIT_FAILURE);
            }
        }
        
        //output redirection
        if (cmnd->output)
        {
            output = open(cmnd->output, O_WRONLY | O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
                    
            if (output < 0)
            {
                perror("error opening output stream:");
                exit(EXIT_FAILURE);
            }
        }
        
        pid = fork();
        
        if (pid == (pid_t)(-1)) {
            perror("Fork error");
            exit(EXIT_FAILURE);
        }
    
        if (pid == (pid_t)0) {
            //this is executed in the child (0 returned)
            spawn(cmnd, input, output);
        }
        else
        {
            //this is executed in the parent
            if (cmnd->backgnd == 1)
            {
                //register a signal handler if spawning backgnd process
                signal(SIGCHLD, child_handler);
                //move the backgnd process into a new process group
                setpgid(0, 0);
            }
            else
            {
                if (waitpid(pid, &status, 0) < 0)
                {
                    perror("waitpid error");
                    exit(EXIT_FAILURE);
                }
            }
        }
        
        if (input != STDIN_FILENO)
            close(input);
        if (output != STDOUT_FILENO)
            close(output);
        input = pipeline[0];
    }
}

pid_t spawn(command* cmnd, int in_fd, int out_fd)
{
    //now this is always executed in the fork'd process, rename into something else!
    
    //set the i/o streams file descriptors before exec. 
    if (in_fd != STDIN_FILENO)
    {
        dup2(in_fd, STDIN_FILENO);
        close(in_fd);
    }
    
    if (out_fd != STDOUT_FILENO)
    {
        dup2(out_fd, STDOUT_FILENO);
        close(out_fd);
    }
        
    execvp(cmnd->name, cmnd->argv);
    //if we ever get here, the exec has failed
    perror("Execution failed");
    exit(EXIT_FAILURE);
}

int free_command(command* cmnd)
{
    //frees the memory used by dynamic structures.
    if (cmnd->next)
        free_command(cmnd->next);
    
    int i = 0;
    free(cmnd->name);
    
    for (i = 0; cmnd->argv[i] != NULL; i++)
        free(cmnd->argv[i]);
    
    free(cmnd->output);
    free(cmnd->input);
    free(cmnd);
    
    return 0;
}

void child_handler(int signum)
{
    int status;
    waitpid(-1, &status, WNOHANG);
}
