// Builtin commands of the shell
// Petteri Mustonen, 2014

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "builtins.h"
#include "uglyshell.h"
#include "parser.h"
#include "history.h"

//prototypes on functions not visible outside this file
int cd(char* path);

int exec_builtin(command* cmnd)
{
    if (strcmp(cmnd->name, "cd") == 0) {
        cd(cmnd->argv[1]);
        return 0;
    }

    else if (strcmp(cmnd->name, "history") == 0) {
        print_history();
        return 0;
    }
    
    else if (strcmp(cmnd->name, "exit") == 0) {
        exit(EXIT_SUCCESS);
    }
    return 1;
}

int cd(char* path)
{
    if (!path)
        path = getenv("HOME");
    
    if (chdir(path) == -1) {
        perror("chdir error");
        return -1;
    }
    return 0;
}


    
