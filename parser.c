// Command line parsing functions of the shell
// Petteri Mustonen, 2014

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "uglyshell.h"
#include "parser.h"
#include "history.h"

command* parse(char* buf)
//parses the command line into list of command-structs
{
    command* cmnd = NULL;
    command* p_cmnd = NULL;
    char* saveptr = NULL;
    char* cmnd_str = NULL;
    char from_history = 0;
    int historynumber;
    
    //check if trying to execute something historical
    if (buf[0] == '!')
    {
        if (sscanf(buf, "!%d", &historynumber) == 0)
        {
            printf("Syntax error.\n");
            return NULL;
        }
        
        buf = get_history(historynumber);
        from_history = 1;
    }        
    
    //parsing of the pipes
    for (cmnd_str = strtok_r(buf, "|", &saveptr); \
        cmnd_str; cmnd_str = strtok_r(NULL, "|", &saveptr))
    {
        if (!cmnd)
        {
            cmnd = parse_cmnd(cmnd_str);
            cmnd->next = NULL;
            p_cmnd = cmnd;
        }
        else
        {
            cmnd->next = parse_cmnd(cmnd_str);
            cmnd = cmnd->next;
            cmnd->next = NULL;
        }
    }
    
    if (from_history)
        free(buf);
    
    return p_cmnd;
}

command* parse_cmnd(char* buf)
//parses the individual commands in "pipeline".
{
    command* cmnd = (command*) malloc(sizeof(command));
    char* name;
    char* arg;
    char* output;
    char* input;
    int i = 1;
    
    //initialize cmnd
    cmnd->name = NULL; cmnd->argv[0] = NULL; cmnd->output = NULL;
    cmnd->input = NULL; cmnd->backgnd = 0; cmnd->next = NULL;
    
    if ((name = strtok(buf, " ")))
    {
        //i am brave enough to use strcpy!
        cmnd->name = (char*)malloc(sizeof(char)*(strlen(name)+1));
        strcpy(cmnd->name, name);
        cmnd->argv[0] = (char*)malloc(sizeof(char)*(strlen(name)+1));
        strcpy(cmnd->argv[0], name);
    }
    
    for (i = 1; (arg = strtok(NULL, " ")); )
    {
        //printf("arg: %s\n", arg);
        switch (*arg) {
            case '>':
                output = strtok(NULL, " ");
                cmnd->output = (char*)malloc(sizeof(char)*(strlen(output)+1));
                strcpy(cmnd->output, output);
                //printf("output: %s\n", output);
                break;
            case '<':
                input = strtok(NULL, " ");
                cmnd->input = (char*)malloc(sizeof(char)*(strlen(input)+1));
                strcpy(cmnd->input, input);
                //printf("input: %s\n", input);
                break;
            case '&':
                cmnd->backgnd = 1;
                break;
            default:
                //printf("default: %s\n", arg);
                cmnd->argv[i] = (char*)malloc(sizeof(char)*(strlen(arg)+1));
                strcpy(cmnd->argv[i], arg);
                i++;
                break;
        }
    }
    
    cmnd->argv[i] = NULL;
    return cmnd;
}
